var requests = [];
var bodies = [];
document.addEventListener('DOMContentLoaded', function() {
    chrome.webRequest.onBeforeSendHeaders.addListener(beforeHeadersSentEvent, { urls: ["<all_urls>"] }, ['requestHeaders', 'blocking']);
    chrome.webRequest.onBeforeRequest.addListener(beforeRequest, { urls: ["<all_urls>"] }, ['requestBody']);
}, false);

chrome.tabs.onActivated.addListener(function(tabId, changeInfo, tab) {
    requests[tabId] = [];
});

function beforeRequest(data) {
    console.log(data.url);
    if (data.method = "POST") {
        if (data.requestBody != undefined) {
            bodies[data.requestId] = JSON.stringify(ab2str(data.requestBody.raw[0].bytes));
        }
    }
}

function beforeHeadersSentEvent(data) {
    if (requests[data.tabId] == undefined) requests[data.tabId] = [];

    var json = { method: data.method, url: data.url, type: data.type, headers: data.requestHeaders, requestId: data.requestId };

    requests[data.tabId].push(json);
};

chrome.browserAction.onClicked.addListener(function(activeTab) {
    var json = requests[activeTab.id];
    for (var i in json) {
        json[i].requestBody = bodies[json[i].requestId];
    }

    var body = {}
    body.user = guid();
    body.requests = json;
    console.log(body);
    $.ajax({
        type: 'POST',
        url: "http://localhost:59889/Home/PostRequest",
        data: JSON.stringify(body),
        success: function(returnPayload) {
            console && console.log("payload: " + returnPayload.Guid);
            chrome.tabs.create({ "url": "http://localhost:59889/Home/Index/" + returnPayload.Guid });
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console && console.log("request failed");
        },
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        processData: false,
        async: true
    });

    /*   chrome.windows.getAll({ populate: true, windowTypes: ["app"] }, function(windows) {
           console.log(windows);
           chrome.webRequest.onBeforeSendHeaders.addListener(beforeHeadersSentEvent, { urls: ["<all_urls>"], windowId: windows[0].id }, ['requestHeaders', 'blocking']);
           chrome.webRequest.onBeforeRequest.addListener(beforeRequest, { urls: ["<all_urls>"], windowId: windows[0].id }, ['requestBody']);
       })*/
});

var decoder = new TextDecoder("utf-8");

function ab2str(buf) {
    return decoder.decode(new Uint8Array(buf));
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}